{pkgs}:
pkgs.python3.withPackages (ps:
    with ps; [
      matplotlib
      numpy
      tkinter
    ])
