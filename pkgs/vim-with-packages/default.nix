{pkgs}:
pkgs.neovim.override {
  configure = {
    packages.myVimPackage = with pkgs.vimPlugins; {
      start = [vim-metamath];
      opt = [];
    };
  };
}
