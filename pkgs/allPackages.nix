_: prev: rec {
  emacs-with-packages = prev.callPackage ./emacs-with-packages/default.nix {};

  ghc-with-packages = prev.callPackage ./ghc-with-packages/default.nix {};

  kara = prev.callPackage ./kara/default.nix {};

  neovim-with-packages = prev.callPackage ./vim-with-packages/default.nix {};

  pmars = prev.callPackage ./pmars/default.nix {};

  python-with-packages = prev.callPackage ./python-with-packages/default.nix {};

  reduce = prev.callPackage ./reduce/default.nix {};

  setmm = prev.callPackage ./metamath-database/default.nix {};
}
