{pkgs}:
(pkgs.emacsPackagesFor pkgs.emacs).emacsWithPackages (epkgs:
    with epkgs; [
      auctex
      paredit
      prolog-mode
    ])
