{pkgs, ...}: {
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    supportedFilesystems = ["exfat" "ext4" "btrfs" "vfat" "fat"];
  };
  zramSwap.enable = true;

  networking = {
    firewall = {enable = true;};
    networkmanager = {
      enable = true;
      wifi = {
        powersave = true;
        backend = "iwd";
      };
    };
    wireless.enable = false;
  };

  console = {keyMap = "de";};

  i18n = {defaultLocale = "de_DE.UTF-8";};

  time.timeZone = "Europe/Berlin";

  hardware = {
    pulseaudio.enable = true;
    enableAllFirmware = true;
  };

  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  environment = {
    systemPackages = with pkgs; [
      chez
      chromium
      emacs-with-packages
      ghc-with-packages
      gimp
      git
      godot
      gprolog
      inkscape
      kara
      kdenlive
      lean
      maxima
      metamath
      neovim-with-packages
      pmars
      python-with-packages
      racket
      reduce
      setmm
      swiProlog
      texlive.combined.scheme-basic
      wget
    ];
  };

  fonts = {
    enableDefaultFonts = true;
  };

  users = {
    mutableUsers = false;
    users.gregor = {
      description = "Gregor";
      initialPassword = "mathecamp";
      isNormalUser = true;
      shell = pkgs.fish;
      group = "users";
    };
  };

  programs = {fish = {enable = true;};};

  services = {
    gnome.gnome-initial-setup.enable = false;
    journald.extraConfig = "Storage=volatile";

    xserver = {
      enable = true;
      layout = "de";
      xkbVariant = "nodeadkeys";
      desktopManager = {
        gnome.enable = true;
        xterm.enable = false;
      };

      displayManager = {
        autoLogin.enable = true;
        autoLogin.user = "gregor";
      };

      libinput = {
        enable = true;
        touchpad.middleEmulation = true;
      };
    };
  };
  system.stateVersion = "22.11";
}
