{
  stdenv,
  lib,
  fetchurl,
  dpkg,
  autoPatchelfHook,
  xorg,
  ncurses5,
  zlib,
}:
stdenv.mkDerivation rec {
  pname = "reduce";
  version = "2022-06-17";

  src = fetchurl {
    url = "mirror://sourceforge/reduce-algebra/snapshot_${version}/linux64/reduce-complete_6339_amd64.deb";
    sha256 = "sha256-MuX7IBejypV9FAiy7pFRwtTRxbmxMYntNW5WMchPaPg=";
  };

  nativeBuildInputs = [
    autoPatchelfHook
    dpkg
  ];

  buildInputs = [
    xorg.libX11
    xorg.libXrandr
    xorg.libXcursor
    xorg.libXft

    ncurses5
    zlib
    stdenv.cc.cc.lib
  ];

  dontUnpack = true;

  installPhase = ''
    mkdir -p $out/bin
    dpkg -x $src $out

    cp -r $out/usr/* $out/

    rm -r $out/usr
  '';

  meta = with lib; {
    homepage = "https://reduce-algebra.sourceforge.io/";
    license = licenses.bsd0;
    description = "portable general-purpose computer algebra system";
    platforms = lib.platforms.linux;
  };
}
