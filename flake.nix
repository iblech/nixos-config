{
  description = "A flake for the NixOS mathecamp live system";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    fup.url = "github:gytis-ivaskevicius/flake-utils-plus";
  };

  outputs = {
    self,
    nixpkgs,
    fup,
  } @ inputs:
    fup.lib.mkFlake {
      inherit inputs self;

      overlays.packages = import ./pkgs/allPackages.nix;
      sharedOverlays = [self.overlays.packages];

      channelsConfig = {allowUnfree = true;};

      hostDefaults = {
        channelName = "nixpkgs";
        system = "x86_64-linux";
        modules = [
          {
            nix.generateRegistryFromInputs = true;
          }
        ];
      };

      hosts = {
        client-vm = {
          modules = [
            ./hosts/client/configuration.nix
            "${nixpkgs}/nixos/modules/virtualisation/qemu-vm.nix"
            {
              users.users.root.password = "";
              virtualisation = {
                memorySize = 4096;
                cores = 4;
              };
            }
          ];
        };

        client-iso = {
          modules = [
            ./hosts/client/configuration.nix
            "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-base.nix"
          ];
        };
      };

      outputsBuilder = channels: {
        packages = fup.lib.exportPackages self.overlays channels;

        devShells.default = channels.nixpkgs.mkShell {
          buildInputs = with channels.nixpkgs; [
            alejandra
            nix-linter
            statix
          ];
        };
      };
    };
}
